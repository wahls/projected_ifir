// ============================================================================
//
//   Copyright 2012 Lehrstuhl f�r Theoretische Informationstechnik,
//   Technische Universit�t M�nchen, Munich, Germany
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// ============================================================================

function h = room_impulse_response(K,L,D)
    
    // The file 30x10y.wav loaded below has been taken from a database of
    // room impulse responses provided by the Centre for Digital Music,
    // Queen Mary, University of London under the terms of the Creative Commons
    // Attribution/Non-commercial/Share-Alike License, Version 3.0. Visit
    // http://creativecommons.org/licenses/by-nc-sa/3.0 or see the included file
    // CC_BY-NC-SA-3.0.txt for more information. Details about the room impulse
    // responses can be be found in the paper
    //
    // R. Steward and M. Sandler: "Database of Omnidirectional and B-Format
    // Impulse Responses", in Proc. IEEE Int. Conf. Acoutics, Speech, Signal
    // Process., Dallas, TX, Mar. 2010
    //
    // The complete collection of room impulse responses is available online at
    // http://isophonics.net/content/room-impulse-response-data-set
    
    h = loadwave("30x10y.wav");
    h = h(800:800+L*(K-1)+D-1)';
    
endfunction