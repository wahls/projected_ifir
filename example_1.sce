// ============================================================================
//
//   Copyright 2012 Lehrstuhl f�r Theoretische Informationstechnik,
//   Technische Universit�t M�nchen, Munich, Germany
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// ============================================================================

mode(-1);
stacksize("max");

// load auxiliary routines

exec loader.sce;

// set simulation parameters (see run_simulation.sci for details)

K = 46;
D = 4;
L = 3;
N = 8000;
mu = 1;
mu_daifir = [0.05 0.1 0.15 0.2];
sig = 1e-1;
del = 1e-3;
eps = 1e-3;
salt = getdate("s");
nruns = 10000;
at_once = 8;

// perform the actual simulation

Es = run_simulation(K,L,D,N,mu,mu_daifir,sig,del,eps,eps,salt,nruns, ...
at_once,random_impulse_response)

// plot the simulation results

plot_results(Es);

// plot ten exemplaric random impulse responses

plot_irs(10,K,L,D,random_impulse_response);
