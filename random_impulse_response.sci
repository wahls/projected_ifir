// ============================================================================
//
//   Copyright 2012 Lehrstuhl f�r Theoretische Informationstechnik,
//   Technische Universit�t M�nchen, Munich, Germany
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// ============================================================================

function h = random_impulse_response(K,L,D)
    
    h = real(ifft([rand(1,L,"normal") zeros(1,L*(K-2)+D)]))';
    b = exp(log(1e-3)/(L*(K-1)+D-1));
    h = h.*b^(0:(L*(K-1)+D-1))';
    h = h/max(abs(h));
    
endfunction