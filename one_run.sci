// ============================================================================
//
//   Copyright 2012 Lehrstuhl f�r Theoretische Informationstechnik,
//   Technische Universit�t M�nchen, Munich, Germany
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// ============================================================================

function E = one_run(run)

    // This is an auxiliary routine for the run_simulation routine. Several
    // variables have to be set in the surrounding namespace before it can be
    // run.

    // output status information

    printf("run [%5g] of [%5g]\n",run,nruns);

    // initialize the random number generator

    rand("seed",salt+run);

    // construct the upsampling matrix S

    S = zeros(L*(K-1)+1,K);
    for k=1:K do
        S((k-1)*L+1,k) = 1;
    end

    // define a function that returns the matrix T_f for any f

    function val = Tf(f)
        val = toeplitz([f ; zeros(L*(K-1),1)],[f(1) zeros(1,L*(K-1))]);
    endfunction

    // define a function that creates the matrix T_Sg for any g

    function val = TSg(g)
        Sg = S*g;
        val = toeplitz([Sg ; zeros(D-1,1)],[Sg(1) zeros(1,D-1)]);
    endfunction

    // create a channel impulse response

    h = impulse_response();

    // set initial values for the algorithms

    sqnrm_x = 0;
    f0 = rand(D,1,"normal");
    f0 = f0/norm(f0);
    g0 = rand(K,1,"normal");
    g0 = g0/norm(g0);
    h0 = TSg(g0)*f0;
    h_nlms = h0;
    f_opnlms = f0;
    f_pnlms = f0;
    g_pnlms = g0;
    h_pnlms = h0;
    f_spnlms = f0;
    g_spnlms = g0;
    w_spnlms = zeros(D,1);
    v_spnlms = zeros(L*K,1);
    h_spnlms = h0;
    f_daifir = ones(1,length(mu_daifir)).*.f0;
    g_daifir = ones(1,length(mu_daifir)).*.g0;
    w_daifir = zeros(D,length(mu_daifir));
    v_daifir = zeros(L*K,length(mu_daifir));
    h_daifir = ones(1,length(mu_daifir)).*.h0;

    // initialize simulation variables

    E = zeros(ndatasets,N);    // matrix of return values
    x = zeros(L*(K-1)+D,1);

    // iterate over the time slots

    for n=1:N do

        // update the data vector and the norm of the data vector (for SP-NLMS)

        sqnrm_x = sqnrm_x - x($)^2;
        x = [rand(1,1,"normal") ; x(1:$-1)];
        sqnrm_x = sqnrm_x + x(1)^2;
        
        // update the desired signal

        d = h'*x + sig*rand(1,1,"normal");

        // NLMS update

        e = d - h_nlms'*x;
        h_nlms = h_nlms + mu/norm(x)^2*e*x;
        E(1,n) = norm(h-h_nlms)^2;

        // P-NLMS update

        e = d - h_pnlms'*x;
        hbar_pnlms = h_pnlms + mu/norm(x)^2*e*x;
        // The following code is *very* slow.
        //
        // g_pnlms = pinv(Tf(f_pnlms)*S)*hbar_pnlms;
        // f_pnlms = pinv(TSg(g_pnlms))*hbar_pnlms;
        //
        // The backslash operator is faster and as the matrices Tf and TSg
        // are tall with (usually) full column rank, both approaches should
        // give the same result (in exact arithmetic). Execute the Scilab
        // command "help backslash" for more information. Thus, we use
        g_pnlms = (Tf(f_pnlms)*S)\hbar_pnlms;
        f_pnlms = (TSg(g_pnlms))\hbar_pnlms;
        h_pnlms = TSg(g_pnlms)*f_pnlms;
        E(2,n) = norm(h-h_pnlms)^2;

        // SP-NLMS update

        w_spnlms = [g_spnlms'/norm(g_spnlms)*x(1:L:L*K) ; w_spnlms(1:$-1)];
        v_spnlms = [f_spnlms'/norm(f_spnlms)*x(1:D) ; v_spnlms(1:$-1)];
        e = d - norm(g_spnlms)*w_spnlms(1:D)'*f_spnlms;
        tmp1 = norm(f_spnlms)/(del^2+norm(f_spnlms)^2)/(eps^2+sqnrm_x);
        tmp2 = norm(g_spnlms)/(del^2+norm(g_spnlms)^2)/(eps^2+sqnrm_x);
        g_spnlms = g_spnlms + mu*tmp1*e*v_spnlms(1:L:L*K);
        f_spnlms = f_spnlms + mu*tmp2*e*w_spnlms;
        h_spnlms = TSg(g_spnlms)*f_spnlms;
        E(3,n) = norm(h-h_spnlms)^2;

        // SN-DAIFIR update, evaluate multiple step sizes mu

        for k=1:length(mu_daifir) do
            w_daifir(:,k) = [g_daifir(:,k)'*x(1:L:L*K) ; w_daifir(1:$-1,k)];
            v_daifir(:,k) = [f_daifir(:,k)'*x(1:D) ; v_daifir(1:$-1,k)];
            e = d - w_daifir(1:D,k)'*f_daifir(:,k);
            g_daifir(:,k) = g_daifir(:,k) + mu_daifir(k)/ ...
            (eps^2+norm(v_daifir(1:L:L*K,k))^2)*e*v_daifir(1:L:L*K,k);
            f_daifir(:,k) = f_daifir(:,k) + mu_daifir(k)/ ...
            (eps^2+norm(w_daifir(:,k))^2)*e*w_daifir(:,k);
            h_daifir(:,k) = TSg(g_daifir(:,k))*f_daifir(:,k);
            E(3+k,n) = norm(h-h_daifir(:,k))^2;
        end

    end

    // reshape the matrix of return values

    E = matrix(E,ndatasets*N,1);  

endfunction
