// ============================================================================
//
//   Copyright 2012 Lehrstuhl f�r Theoretische Informationstechnik,
//   Technische Universit�t M�nchen, Munich, Germany
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// ============================================================================

function Es = run_simulation(K,L,D,N,mu,mu_daifir,sig,del,eps,eps,salt, ...
    nruns,at_once,impulse_response)

    // This function repeatedly performs simulation runs (in parallel, if
    // posible) by calling the one_run routine, and returns the averaged
    // results. The parameters are:
    //
    //     K: length of g(n)
    //     L: upsampling factor
    //     D: length of f(n)
    //     N: number of time slots to be simulated
    //     mu: step size for NLMS, P-NLMS, SP-NLMS
    //     mu_daifir: vector of step sizes for SN-DAIFIR
    //     sig: sig^2 is the noise power
    //     del: first regularization constant
    //     eps: second regularization constant
    //     salt: used for the initialization of the random number generator
    //     nruns: number of simulation runs
    //     at_once: maximal number of of simulation runs to be averaged over
    //     (larger values need more memory, the number of cpu cores suffices,
    //     multicore support in Scilab 5.3.3: only under Linux)
    //     impulse_response: function of the form h=f(K,L,D) that returns a
    //     L*(K-1)+D column vector

    printf("\nstarting simulation ...\n\n");

    // Note: The parameters are available in the one_run routine although we
    // do not explicitly pass them. For details, visit
    // http://wiki.scilab.org/howto/global%20and%20local%20variables

    tic();
    ndatasets = 3 + length(mu_daifir);
    Es = zeros(ndatasets*N,at_once);
    for k=1:at_once:nruns do
        num = min(nruns-k+1,at_once);
        Es(:,1:num) = Es(:,1:num) + ...
        parallel_run(k:k+num-1,one_run,ndatasets*N);
    end
    Es = matrix(sum(Es,"c")/nruns,ndatasets,N);
    t = toc();

    printf("\nruntime: %g sec\n\n",t);

endfunction
