// ============================================================================
//
//   Copyright 2012 Lehrstuhl f�r Theoretische Informationstechnik,
//   Technische Universit�t M�nchen, Munich, Germany
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// ============================================================================

function plot_results(Es)

    // This function plots the simulation results created by the run_simulation
    // routine.

    f = scf();
    f.figure_size = [614 544];
    a = gca();
    a.font_size = 4;
    a.font_style = 2;
    a.x_label.font_size = 4;
    a.x_label.font_style = 2;
    a.y_label.font_size = 4;
    a.y_label.font_style = 2;
    styles = ["-k" "-g" "-r" "-b" "-m" "-c" "-y"];
    for k=1:length(Es(:,1)) do
        plot(Es(k,:),styles(1+pmodulo(k-1,size(styles,"*"))));
    end
    a.log_flags = "nln";
    a.data_bounds(1,2) = min(Es(2:$,:));
    a.data_bounds(2,2) = Es(1,1);
    //strs = ["NLMS" ; "P-NLMS" ; "SP-NLMS"];
    //for mu=mu_daifir do
    //    strs = [strs ; sprintf("SN-DAIFIR (mu=%g)",mu)];
    //end
    //legend(strs);
    ylabel("${\text{Avg. } \Vert {\bf \bar{h}}-{\bf h}(n)\Vert^2" ...
    +"\text{ over }n}$");

endfunction