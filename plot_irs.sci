// ============================================================================
//
//   Copyright 2012 Lehrstuhl f�r Theoretische Informationstechnik,
//   Technische Universit�t M�nchen, Munich, Germany
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// ============================================================================

function plot_irs(num,K,L,D,impulse_response)
    
    // This function creates a plot containing exemplaric impulse responses.
    // The parameters are:
    //
    //    num: number of impulse responses to be plotted
    //    K: length of the filter g(n)
    //    L: upsampling factor
    //    D: length of the filter f(n)
    //    impulse_response: function of the form h=f(K,L,D) that returns a
    //    L*(K-1)+D column vector

    // create the figure

    f = scf();
    f.figure_size = [614 300];
    a = gca();
    a.font_size = 4;
    a.font_style = 2;
    a.x_label.font_size = 4;
    a.x_label.font_style = 2;
    a.y_label.font_size = 4;
    a.y_label.font_style = 2;
    ylabel("${[{\bf \bar{h}}]_k\text{ over }k}$");

    // compute ten random impulse responses

    hs = [];
    for k=1:num do
        h = impulse_response(K,L,D);
        hs = [hs h];
    end

    // plot the impulse responses

    p = plot(hs);

endfunction