This archive contains the source code of the numerical examples in the paper
    
    "A Projection Approach to Adaptive IFIR Filtering"
    
by S. Wahls and H. Boche, which has been accepted for presentation at the 37th
IEEE International Conference on Acoustics, Speech, and Signal Processing
(ICASSP) in Kyoto, Japan, on March 25 - 30, 2012.

The simulations have been performed using the 5.3.3 Linux version of Scilab,
which is an open source platform for numerical computation available online at
http://www.scilab.org.

In order to run the simulation, extract the contents of this archive into some
directory, start Scilab, and execute one of the scripts example_1.sce and
example_2.sce by running the Scilab command "cd DIR ; exec example_X.sce". Here,
DIR has to be replaced with the path of the actual directory that contains the
contents of this archive, and X has to be replaced with 1 or 2, respectively.

The source code in this archive have been written by Sander Wahls as an employee
of the Chair of Theoretical Information Technology at the Technische Universit�t
M�nchen in Munich, Germany. The code is available under the terms of the Apache
License, Version 2.0. Please see the accompanying file APACHE_LICENSE_2.0.txt or
visit http://www.apache.org/licenses/LICENSE-2.0 for more information.

The file 30x10y.wav is distributed under the terms of the Creative Commons
Attribution/Non-Commercial/Share-Alike License, Version 3.0, with attribution to
the Centre for Digital Music, Queen Mary, University of London. Please see the
accompanying file CC_BY-NC-SA_3.0.txt or visit
http://creativecommons.org/licenses/by-nc-sa/3.0 for more information.
The file is part of a database of room impulse responses described in the paper
"Database of Omnidirectional and B-Format Room Impulse Responses" by R. Steward
and M. Sandler, Proc. 35th IEEE Int. Conf. Acoustics, Speech, Signal Process.
(ICASSP), Dallas, TX, Mar. 2010. The complete database can be downloaded from
http://isophonics.net/content/room-impulse-response-data-set.

